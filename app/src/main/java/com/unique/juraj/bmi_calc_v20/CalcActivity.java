    package com.unique.juraj.bmi_calc_v20;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

    public class CalcActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc);

        final Button btnIzracun = (Button) findViewById(R.id.btnIzracun);
        final EditText etTezina = (EditText) findViewById(R.id.etTezina);
        final EditText etVisina = (EditText) findViewById(R.id.etVisina);
        final TextView tvmsg = (TextView) findViewById(R.id.tvmsg);
        final TextView tvres = (TextView) findViewById(R.id.tvres);
        final ImageView ivRez = (ImageView) findViewById(R.id.ivRez);

        btnIzracun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double tezina;
                double visina;
                double visinaCm;
                double rezultat;
                String msg="";



                tezina=Double.parseDouble(etTezina.getText().toString());
                visina=Double.parseDouble(etVisina.getText().toString());

                visinaCm = visina / 100;
                rezultat = tezina / (visinaCm * visinaCm);


                tvres.setText(String.valueOf(rezultat));

                if(rezultat < 18.5){
                    msg= "Pothranjen :(";


                }else if (rezultat > 18.5 && rezultat < 25){
                    msg = "Zdrav :)";
                }else if(rezultat > 25 && rezultat <40){
                    msg = "Debeo";
                }else if (rezultat > 40){
                    msg = "Pretil";
                }
                tvmsg.setText(msg);

            }
        });

    }
}
